package BridgePattern;

public class ConvertEuro implements Exchange {
    int euro = 20;

    @Override
    public void convertVal(int numeral){
    System.out.println( + numeral + " Euro = " + (numeral * euro) + " Lei ");
    }
}
