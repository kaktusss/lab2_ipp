package BridgePattern;

public interface Exchange {
    public void convertVal(int numeral);
}
