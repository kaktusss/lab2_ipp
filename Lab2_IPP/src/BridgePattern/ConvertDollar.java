package BridgePattern;

import static java.lang.System.out;

public class ConvertDollar implements Exchange{
    int dollar = 17; //в леях
    @Override
    public void convertVal(int numeral){
        out.println( + numeral + " Dollars = " + (numeral * dollar) + " Lei ");
    }
}
