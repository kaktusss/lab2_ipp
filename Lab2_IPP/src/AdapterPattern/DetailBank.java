package AdapterPattern;

import java.util.Random;

public class DetailBank extends Bank implements GetCredit{
    private int branch;
    private int salary;
    private final String SPACE = ", ";

    public DetailBank(String nameBank, String nameCredit, int branch, int salary){
        super(nameBank, nameCredit);
        this.branch = branch;
        this.salary = salary;
    }

    public int getBranch(){
        return branch;
    }

    public int getSalary(){
        return salary;
    }

    @Override
    public int calculateCredit() {
        int sumCredit = 0;
        if(salary > 500 && salary < 1000){
            sumCredit += randomNumber(1000, 500);
        } else if(salary > 1000 && salary < 2000) {
            sumCredit += randomNumber(2000, 1000);
        } else if(salary < 500){
            sumCredit = 0;
        }
        return sumCredit;
    }

    int randomNumber(int max, int min){
        int ranndomNumber = new Random().nextInt(max - min) + min;
        return ranndomNumber;
    }

    @Override
    public String gettingCredit() {
        return getNameBank() + SPACE + getNameCredit() + SPACE + getBranch() + SPACE + getSalary() + SPACE + calculateCredit();
    }
}
