package AdapterPattern;

public class Bank {
    private String nameBank;
    private String nameCredit;
    public Bank(String nameBank, String nameCredit) {
        this.nameBank = nameBank;
        this.nameCredit = nameCredit;
    }
    public String getNameBank() {
        return nameBank;
    }
    public String getNameCredit() {
        return nameCredit;
    }
}