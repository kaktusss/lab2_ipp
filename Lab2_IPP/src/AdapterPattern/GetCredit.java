package AdapterPattern;

public interface GetCredit {
    int calculateCredit();
    String gettingCredit();
}
