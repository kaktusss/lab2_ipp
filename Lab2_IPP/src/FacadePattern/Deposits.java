package FacadePattern;

public interface Deposits {
    int calcPercent();
    String infoDeposits();
}
