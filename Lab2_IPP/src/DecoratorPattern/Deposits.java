package DecoratorPattern;

public class Deposits extends CompBankDecor {
    private String SPACE = ", ";
    private String infoDeposits = "Section Deposits";
    public Deposits(CompBank compBank) {
        super(compBank);
    }
    public String InfoBank() {
        return compBank.InfoBank() + deposits();
    }
    private String deposits() {
        return infoDeposits + SPACE;
    }
}
