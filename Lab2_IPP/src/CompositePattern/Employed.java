package CompositePattern;

public interface Employed {
    int getNrBranch();
    String getName();
    void print();
    void add (Employed employed);
}
